jQuery(document).ready(function($) {
  $('.slider').owlCarousel({
     navigation : true, // Show next and prev buttons
     pagination : true,
     slideSpeed : 300,
     paginationSpeed : 400,
     navigationText : ['<i class="fa fa-angle-left"></i>','<i class="fa fa-angle-right"></i>'],
     autoPlay : true,
     singleItem:true,
     items:1,
     //Nevigation & Pagination owl thme
   // theme:"square", //round/roundTopLeft/roundTopRight/roundBottomLeft/roundBottomCenter/roundBottomRight/
    theme:"square"    //square/squareTopLeft/squareTopRight/squareBottomLeft/squareBottomCenter/squareBottomRight
          //pagi_round/pagi_square/pagi_squareLeft
     

  // itemsCustom : false,
    // itemsDesktop : [1199,4],
    // itemsDesktopSmall : [980,3],
    // itemsTablet: [768,2],
    // itemsTabletSmall: false,
    // itemsMobile : [479,1],

  });

  $('.slider_photogallery').owlCarousel({
     navigation : true, // Show next and prev buttons
     pagination : true,
     slideSpeed : 300,
     paginationSpeed : 400,
     navigationText : ['<i class="fa fa-angle-left"></i>','<i class="fa fa-angle-right"></i>'],
     autoPlay : false,
     singleItem:true,
     items:1,
     //Nevigation & Pagination owl thme
   // theme:"square", //round/roundTopLeft/roundTopRight/roundBottomLeft/roundBottomCenter/roundBottomRight/
    theme:"pagi_round"    //square/squareTopLeft/squareTopRight/squareBottomLeft/squareBottomCenter/squareBottomRight
          //pagi_round/pagi_square/pagi_squareLeft
     
  });
});